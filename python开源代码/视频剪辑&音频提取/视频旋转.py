from moviepy.editor import *

aviFileName = r'./video/video3.mp4'
resultFileName = r'./result/3-0.mp4'
video = VideoFileClip(aviFileName)
size = video.w, video.h
sub_videos = [video.subclip(0, 18).rotate(90).resize(size)]  # 逆时针旋转
'''
              video.subclip(1, 2).rotate(-90).resize(size),
              video.subclip(2, 3).rotate(3.1415926, unit='rad').resize(size),
              video.subclip(3, 4).rotate(60, resample='nearest').resize(size),
              video.subclip(1, 2).rotate(-60, resample='bilinear').resize(size)
'''

concatenate_videoclips(sub_videos).write_videofile(resultFileName)